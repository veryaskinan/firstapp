import 'package:flutter/material.dart';
import 'Screens/Home.dart';
import 'Screens/Profile.dart';
import 'Screens/Login.dart';
import 'package:firstapp/AppState.dart';

class Navigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      selectedFontSize: 0,
      backgroundColor: Colors.blue,
      items:[
        BottomNavigationBarItem(icon: Icon(Icons.home, color: Colors.white,), label: '', backgroundColor: Colors.blue),
        BottomNavigationBarItem(icon: Icon(Icons.accessibility, color: Colors.white,), label: '', backgroundColor: Colors.blue),
      ],
      onTap: (index) {
        switch (index) {
          case 0:
            Navigator.push(
                context,
                PageRouteBuilder(
                  pageBuilder: (_, __, ___) => homeScreen,
                  transitionDuration: Duration(seconds: 0),
                )
            );
            AppState().screen = homeScreen;
            break;
          case 1:
            Navigator.push(
                context,
                PageRouteBuilder(
                  pageBuilder: (_, __, ___) => AppState().jwtAccess == '' ? loginScreen : profileScreen,
                  transitionDuration: Duration(seconds: 0),
                )
            );
            AppState().screen = profileScreen;
            break;
        }
      },
    );
  }
}