import 'package:flutter/material.dart';
import '../AppBar.dart';
import '../Navigator.dart';

class CommonScreen extends StatelessWidget {
  String name;
  Widget body;

  CommonScreen(String name, Widget body) {
    this.name = name;
    this.body = body;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: myAppBar,
      body: this.body,
      bottomNavigationBar: Navigation(),
    );
  }
}