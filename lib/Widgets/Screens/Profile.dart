import 'package:flutter/material.dart';
import 'package:firstapp/Widgets/Screens/Common.dart';

var profileScreen = CommonScreen('Profile',
    Center(
      child: Container(
          child: Icon(
            Icons.cloud, size: 40, color: Colors.white,
          ),
          alignment: Alignment.center,
          width: 200,
          height: 200,
          decoration: BoxDecoration(
              color: Colors.purple[300],
              borderRadius: BorderRadius.all(Radius.circular(350))
          )
      ),
    )
);