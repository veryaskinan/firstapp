import 'package:flutter/material.dart';
import 'package:firstapp/AppState.dart';

class MyAppBar extends StatefulWidget implements PreferredSizeWidget{
  @override
  createState() => new MyAppBarState();

  @override
  Size get preferredSize => Size.fromHeight(50);
}

class MyAppBarState extends State<MyAppBar>  {

  @override
  initState() {
    super.initState();
    // ...
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(AppState().screen.name, style: TextStyle(color: Colors.white),),
    );
  }
}

var myAppBar = MyAppBar();