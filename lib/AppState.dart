import 'package:flutter/material.dart';
import 'package:firstapp/Widgets/Screens/Common.dart';
import 'package:firstapp/Widgets/Screens/Home.dart';

class AppState {
  static final AppState _singleton = AppState._internal();
  factory AppState() => _singleton;
  AppState._internal();

  String jwtAccess = '';
  CommonScreen screen = homeScreen;
}